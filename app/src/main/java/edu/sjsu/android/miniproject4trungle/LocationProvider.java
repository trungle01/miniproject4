package edu.sjsu.android.miniproject4trungle;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;

public class LocationProvider extends ContentProvider {
    // provide a public static Uri
    public static String AUTHORITY = "edu.sjsu.android.miniproject4trungle.LocationProvider";
    public static Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    // a private LocationsDB object
    private LocationsDB db;
    @Override
    public boolean onCreate() {
        // construct the LocationDB object
        db = new LocationsDB(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return db.deleteAll();
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // Implement this to handle requests to insert a new row.
        long rowID = db.insert(values);
        // If record is added successfully
        if(rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(uri, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        return db.getAllLocations();
    }

    // -----------
    // Following methods won't be used in this project
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
