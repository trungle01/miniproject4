package edu.sjsu.android.miniproject4trungle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.sjsu.android.miniproject4trungle.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor>{

    private LocationsDB database;
    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    private final String AUTHORITY = "edu.sjsu.android.miniproject4trungle.LocationProvider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    // (Extra credit) SharedPreferences and KEYs needed

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMapsBinding.inflate(getLayoutInflater());

        binding.city.setOnClickListener(this::switchView);
        binding.univ.setOnClickListener(this::switchView);
        binding.cs.setOnClickListener(this::switchView);
        binding.uninstall.setOnClickListener(this::uninstall);
        binding.location.setOnClickListener(this::getLocation);
        database = new LocationsDB(this);
        setContentView(binding.getRoot());
        // retrieve and draw already saved locations in map
        // Hint: a method in LoaderManager
//        LoaderManager.getInstance(this).initLoader(0, null, this);
        // TODO: extra credit - restore the map setting
        //  (camara position & map type) using SharedPreferences

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(point -> {
            mMap.addMarker(new MarkerOptions().position(point));
            ContentValues values = new ContentValues();
            values.put(LocationsDB.LATITUDE, point.latitude);     // what we set in LocationsDB
            values.put(LocationsDB.LONGITUDE, point.longitude);
            values.put(LocationsDB.ZOOMLEVEL, mMap.getCameraPosition().zoom);
            new MyTask().execute(values);
        });

        mMap.setOnMapLongClickListener(point -> {
            mMap.clear();
            // 2) Call execute() on a MyTask (defined below) object to clear the database
            new MyTask().execute();
            Toast.makeText(this, "All markers are removed", Toast.LENGTH_SHORT).show();
        });
        mMap.addMarker(new MarkerOptions().position(LOCATION_CS).title("find me here"));
    }

    // Below is the class that extend AsyncTask, to insert/delete data in background
    // Note that AsyncTask is deprecated from API 30, but you can still use it.
    // You can use java.util.concurrent instead, if you are familiar with threads and concurrency.
    private class MyTask extends AsyncTask<ContentValues, Void, Void> {
        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            // insert one row or delete all data base on input
            // Hint: if the first contentValues is not null, insert it
            // Otherwise, delete all.
            // Both operations should be done through content provider
            if(contentValues != null){
                getContentResolver().insert(CONTENT_URI, contentValues[0]);
            }else{
                getContentResolver().delete(CONTENT_URI, null,null);
            }
            return null;
        }
    }
    // ----- End of AsyncTask classes -----


    // Below are for the CursorLoader, that is, the methods of
    // LoaderManager.LoaderCallbacks<Cursor> interface
    /**
     * Instantiate and return a new Loader for the database.
     *
     * @param id   the ID whose loader is to be created
     * @param args any arguments supplied by the caller
     * @return a new Loader instance that is ready to start loading
     */
    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    /**
     * Draw the markers after data is loaded, that is, the loader returned
     * in the onCreateLoader has finished its load.
     *
     * @param loader the Loader that has finished
     * @param cursor a cursor to read the data generated by the Loader
     */
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        double lat, lng;
        float zoom;
        // First, get data row by row using the cursor
        if(cursor.moveToFirst()){
            do{
                // For each row, get the latitude, longitude and zoom level
                // Draw a marker based on the LatLng object on the map
                lat = Double.parseDouble(cursor.getString(0));
                lng = Double.parseDouble(cursor.getString(1));
                zoom = Float.parseFloat(cursor.getString(2));
                LatLng local_location = new LatLng(lat, lng);
                mMap.addMarker(new MarkerOptions().position(local_location).title("find me here"));
            }while (cursor.moveToNext());
            // After getting all data, move the "camera" to focus on the last clicked location
            // Also the zoom level should be the same as the last clicked location
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        // No need to implement
    }
    // ----- End of LoaderManager.LoaderCallbacks<Cursor> methods -----


    // Below are the methods respond to clicks on buttons
    // Remember to attach them to the buttons in onCreate
    // getLocation and switchView are the same as the ones in exercise 6.
    public void uninstall(View view) {
        Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse("package: " + getPackageName()));
        startActivity(intent);
    }

    public void getLocation(View view) {
        GPSTracker tracker = new GPSTracker(this);
        tracker.getLocation();
    }

    public void switchView(View view) {
        CameraUpdate update = null;

        if(view.getId() == R.id.city){
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 10f);
        }else if(view.getId() == R.id.univ){
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV, 14f);
        }else if(view.getId() == R.id.cs){
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            update = CameraUpdateFactory.newLatLngZoom(LOCATION_CS, 18f);
        }
        mMap.animateCamera(update);
    }

}